<?php

use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Creating students registered in CSM'
         */
        $CSM = App\SchoolBoard::where('name', 'CSM School Board')->first();

        $student = new App\Student;
        $student->name = 'John Doe';
        $student->grades = '{"Math": 9, "History": 7, "English": 6, "French": 5}'; 
        $student->schoolBoard_id = $CSM->id;
        $student->save();

        $student = new App\Student;
        $student->name = 'Matt Rock';
        $student->grades = '{"Math": 8, "History": 4, "English": 4, "French": 5}'; 
        $student->schoolBoard_id = $CSM->id;
        $student->save();

        $student = new App\Student;
        $student->name = 'Bill Matis';
        $student->grades = '{"Math": 6, "History": 4, "English": 3}'; 
        $student->schoolBoard_id = $CSM->id;
        $student->save();

        /**
         * Creating students registered in CSMB'
         */

        $CSMB = App\SchoolBoard::where('name', 'CSMB School Board')->first();

        $student = new App\Student;
        $student->name = 'Maria John';
        $student->grades = '{"Math": 8, "History": 5, "English": 2, "French": 1}'; 
        $student->schoolBoard_id = $CSMB->id;
        $student->save();

        $student = new App\Student;
        $student->name = 'Ted Moore';
        $student->grades = '{"Math": 8, "History": 4, "English": 4, "French": 5}'; 
        $student->schoolBoard_id = $CSMB->id;
        $student->save();

        $student = new App\Student;
        $student->name = 'Natasha Uriskov';
        $student->grades = '{"Math": 4, "History": 6, "English": 5, "French": 6}'; 
        $student->schoolBoard_id = $CSMB->id;
        $student->save();

        $student = new App\Student;
        $student->name = 'Yu Chen';
        $student->grades = '{"Math": 6, "History": 5, "English": 4, "French": 3}'; 
        $student->schoolBoard_id = $CSMB->id;
        $student->save();
    }
}
