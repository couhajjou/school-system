<?php

use Illuminate\Database\Seeder;

class SchoolBoardTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $CSM = new App\SchoolBoard;
        $CSM->name = 'CSM School Board';
        $CSM->average = 'CSM';
        $CSM->format = 'json';
        $CSM->save();

        $CSMB = new App\SchoolBoard;
        $CSMB->name = 'CSMB School Board';
        $CSMB->average = 'CSMB';
        $CSMB->format = 'xml';
        $CSMB->save();

        assert(App\SchoolBoard::all()->count() == 2);
    }
}
