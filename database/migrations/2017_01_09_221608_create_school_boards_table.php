<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_boards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            // method used to calculate the grade average for a student
            $table->enum('average', ['CSM', 'CSMB']); 
            // format required by the school board
            $table->enum('format', ['xml', 'json']);  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_boards');
    }
}
