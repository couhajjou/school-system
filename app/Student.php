<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * Get the school board where the student is registred
     */
    public function schoolBoard()
    {
        return $this->belongsTo('App\SchoolBoard');
    }
}
