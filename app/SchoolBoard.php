<?php
// TODO: This file needs some refactoring

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolBoard extends Model
{
    /**
     * Computes the result (average and passed) given a student grade. Uses the CSM Method
     *
     * @param grades an array with of (subject => grades)
     * @return array (average => float, passed => boolean)
     */
    private function resultWithCSM($grades) {
        $sum = 0;
        $count = count($grades);
        foreach($grades as $grade) {
            $sum += $grade;
        }
        $average = $sum / $count;
        $pass = $average >= 7;

        return array("average" => $average, "pass" => $pass);
    }

    /**
     * Computes the result (average and passed) given a student grade. Uses the CSMB Method
     *
     * @param grades an array with of (subject => grades)
     * @return array (average => float, passed => boolean)
     */
    private function resultWithCSMB($grades) {
        $sum = 0;
        $count = count($grades);
        $smallestGrade = null; 
        foreach($grades as $grade) {
            $sum += $grade;

            // find the smallest grade
            if(!$smallestGrade || $grade < $smallestGrade) {
                $smallestGrade = $grade;
            }
        }

        // discard the smallest grade if we have more then 2 grades
        if($count >= 2) {
            $sum -= $smallestGrade;
            $count--;
        }

        $average = $sum / $count;
        return array("average" => $average, "pass" => true);
    }

    /**
     * Computes the result (average and passed) given a student grade. Using the appropriate Method
     *
     * @param grades an array with of (subject => grades)
     * @return array (average => float, passed => boolean)
     */
    public function computeResult(Student $student) {
        $grades = (array) json_decode($student->grades);

        switch ($this->average) {
        case "CSM": 
            return $this->resultWithCSM($grades);
        case "CSMB":
            return $this->resultWithCSMB($grades);
        default:
            return null;
        }
    }

    /**
     * Computes the results for all students in the the SchoolBoard
     *
     * @param grades and array with of (subject => grades)
     */
    public function computeResultsARRAY() {
        $students = Student::where('schoolBoard_id', $this->id)->get();
        $lines = array();
        foreach($students as $student) {
            $result = $this->computeResult($student);
            $line = $result;
            $line["student_id"] = $student->id;
            $line["name"] = $student->name;
            $line["grades"] = (array)json_decode($student->grades);

            array_push($lines, $line);
        }

        return $lines;
    }

    /**
     * Like computeResultsARRAY but return results as JSON
     */
    public function computeResultsJSON() {
        return json_encode($this->computeResultsARRAY());
    }

    /**
     * Like computeResultsARRAY but return results as XML
     */
    public function computeResultsXML() {
        $lines = $this->computeResultsARRAY();
        $xml = new \SimpleXMLElement('<root/>');
        foreach($lines as $line => $v) {
            $studentXml = $xml->addChild('student');
            $studentXml->addChild('id', $v["student_id"]);
            $studentXml->addChild('name', $v["name"]);
            $studentXml->addChild('average', $v["average"]);
            $studentXml->addChild('pass', $v["pass"]);
            $gradesXml = $studentXml->addChild('grades');
            foreach($v["grades"] as $subject => $grade) {
                $gradesXml->addChild($subject, $grade);
            }
        }
        return $xml->asXML();
    }

    /**
     * Compute the results and returns the expected format
     */
    public function computeResults() {
        switch ($this->format) {
        case "xml": 
            return $this->computeResultsXML();
        case "json":
            return $this->computeResultsJSON();
        default:
            return null;
        }
    }

}
