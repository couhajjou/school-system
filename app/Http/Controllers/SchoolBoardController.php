<?php

namespace App\Http\Controllers;

use App\Student;
use App\SchoolBoard;
use Illuminate\Http\Request;

class SchoolBoardController extends Controller
{
    /**
     * Show the list of students registred in the given school board.
     *
     * @param  int  $id 
     */ 
    public function students($id) {
        return Student::where('schoolBoard_id', $id)->get();
    }

    /**
     * Show student grades. For the students registred in the given school board.
     *
     * @param  int  $id 
     */ 
    public function results($id) {
        $schoolBoard = SchoolBoard::find($id);

        return response($schoolBoard->computeResults(), 200)
            ->header('Content-Type', 'text/' . $schoolBoard->format);
    }
}
